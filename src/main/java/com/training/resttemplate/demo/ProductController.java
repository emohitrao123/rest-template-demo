package com.training.resttemplate.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductController {

    @PostConstruct
    public void intializeProducts(){
        ProductModel product = new ProductModel();
        product.setName("Refrigerator");
        product.setId(1);
        productMap.put(product.getId(),product);

        ProductModel product1 = new ProductModel();
        product1.setName("Air Conditioner");
        product1.setId(2);
        productMap.put(product1.getId(),product1);


    }

    Map<Integer,ProductModel> productMap = new HashMap<>();

    //Read Method - to read all Products

    @GetMapping("/products")
    public Collection<ProductModel> listOfProducts(){
        return productMap.values();
    }

    /*@GetMapping("/products/{id}")
    public Collection<ProductModel> getProductsById(@PathVariable Integer id){
        ProductModel productModel = new ProductModel();
        for(productModel -> )

    }*/

    // insert more products
    @PostMapping("/products")
    public ResponseEntity<Object> addProducts(@RequestBody ProductModel product){
            productMap.put(product.getId(),product);
            return new ResponseEntity<>("Product is added successfully", HttpStatus.OK);
}

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deleteProductById(@PathVariable Integer id){
        productMap.remove(id);
        return new ResponseEntity<>("Product with"+id+"is deleted succesfully",HttpStatus.OK);
    }

    //update for a specific id
    @PostMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable Integer id, @RequestBody ProductModel product){
        productMap.remove(id);
        product.setId(id);
        productMap.put(id,product);
        return new ResponseEntity<>("Product with "+id+" is replaced", HttpStatus.OK);
    }

}
