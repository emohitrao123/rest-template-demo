package com.training.resttemplate.demo;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;

@RestController
public class ProductRestTemplateController {

    RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/products/template")
    public String getProductByTemplate(){
        HttpHeaders headers = new HttpHeaders(); //setting up of header
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("https://weather.com/weather/tenday/l/11220:4:US", HttpMethod.GET,entity,String.class).getBody();
    }

}
