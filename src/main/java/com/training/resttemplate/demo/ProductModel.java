package com.training.resttemplate.demo;

public class ProductModel {

    public String name;

    public Integer id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductModel(String name, Integer id) {
        this.name = name;
        this.id = id;
    }

    protected ProductModel() {
    }
}
